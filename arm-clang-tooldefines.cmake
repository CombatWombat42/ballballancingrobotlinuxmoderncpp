set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_C_FLAGS "${ARM_INCLUDE_DIRS}")
set(CMAKE_C_COMPILER clang${clang-version})

set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_CXX_COMPILER clang++${clang-version})
set(CMAKE_CXX_FLAGS "${ARM_INCLUDE_DIRS}")
set(CMAKE_CXX_COMPILER_TARGET ${triple})

set(CMAKE_LINKER lld${clang-version})
set(CMAKE_EXE_LINKER_FLAGS "-fuse-ld=lld${clang-version} -static")

set(CMAKE_AR "llvm-ar${clang-version}")