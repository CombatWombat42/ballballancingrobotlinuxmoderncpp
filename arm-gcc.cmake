set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(gcc_version "-9")

set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc${gcc_version})
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++${gcc_version})
