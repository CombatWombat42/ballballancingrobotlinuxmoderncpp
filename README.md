
# Overview

This project is to create a robot that can ballance on top of a basketball or other simularly sized ball.

* I intend to use stepper motors with omniwheels mounted in a triangular configuration to drive the robot on top of the ball.
* I intend to use the accelerometer and gyrometer that are part of the LSM9DS0 to sense the robots postion.
* I intend to use a PID to close the loop between the sensors and accelerometers

## Other "branches"

* I have brushed DC motors that I can also attach the omniwheels to, these require more control and likely will need indivitual feedback from an encoder
* The STM32 controllers have built in encoder counters, I may create a branch of this design that uses an STM32 to controll the motors with H-bridges and encoder counters

## Hardware

This project is intended to be run on a Raspberry PI 4. I was origonally targeting the Beaglbone Black/Green as it had 3 hardware PWM channels as opposed to the PI's 2, but as discussed above, I intend to use steppers (which don't require PWM), or a microcontroller to create servomotors (and communicate with the microcontroler over something like SPI)

### Wiring

 I use some 2n3904's wired like [this](https://www.raspberrypi.org/forums/viewtopic.php?t=242928) (emitter grounded, base connected to pi through 1kOhm (brown black red), collector connected to negative pin of signal on TB6600, 5V connected to positive pin of TB6600 signal)

 | Signal name       | BCM GPIO number  | Pi pin number |
 |-------------------|------------------|---------------|
 | Motor 1 step      | 17               | 11            |
 | Motor 1 direction | 4                | 7             |
 | Motor 2 step      | 6                | 31            |
 | Motor 2 direction | 27               | 13            |
 | Motor 3 step      | 13               | 33            |
 | Motor 3 direction | 5                | 29            |

### Power supplies

I currerntly have access to the 12V 3A SMPS in mly "big box of power" and my old ATX 430W that is rated for 20A on the 12V line. No matter what when I set the switches on the TB6600's to 3.5A (s4 off s5 off s6 off) nothing can handle it. That makes sense for the "big box of power" but the ATX should be able to handle it. I suspect it's the wiring I'm using. I doubt it's rated for 12A I should investigate further, but in the mean time I have orderd a [board](https://www.amazon.com/gp/product/B07SV4KZ4B/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) that should do better assuming I use a reasonable gage of wire.

Note that I have also set all TB6600's to 2A (s4 on s5 off s6 off)

## Scheduler

All of these controlls are dependant on having fairly low jitter, and in many cases low latency. I think I can use SCHED_DEADLINE and would perfer to not patch the kernel with the prempt rt patches as I think I can get away without them.

## SPI

I intend to use the SPI interface, I have played with the userspace SPI inerface [here](https://bitbucket.org/CombatWombat42/sched_deadline_testing/src/master/src/sensor.hpp) and will likely adapt that class to read the accelerometer and gyro.

## Sensor

I am using the LSM9D0 although that particular chip is no longer avalible so if something goes wrong I wil probably switch to any one of the other 9DoF IMU's I have avalbile. I have tested reading the interface at 10KHz if i remember correctly and earlier versions of the "sched_deadling_testing" project are actually able to produce impressive results.

## GPIO

I intend to use /dev/mem to directly access the GPIO for the PI, I ripped off an example and put it [here](https://bitbucket.org/CombatWombat42/sched_deadline_testing/src/master/src/gpio.cpp). Notably I had to find the correct base address for the BCM2711, which appears to be `0xFE000000` and the GPIO controller is an additional `0x200000` off of that.

> :warning: You need to run scripts/setup_gpio.sh to set all the gpio to output

## Building

### Helpful build tips

`cmake .. -DCMAKE_BUILD_TYPE=Debug` turns on debugging symobls automatically probably need to remember to turn this off for thread tuning

### Building for pi

Because I need fast builds, and I havn't worked out the issues with cross compliation I am building "bad" executibles (ones that cause the below issue) in `build/` and building nativly on the pi in `build_on_pi` this keeps me from having to reconfigure the cmake every time.

### Cross compilation

I tried to get cross compliation working but am still struggeling. I ended up static linking but even that causes a really obscure issue `terminate called after throwing an instance of 'std::system_error'` cause by thread spawning.

Presumably I can use the raspi tools if I set up a cmake_tooldefine for it but it bothers me that i can't get the debian gcc-arm-linux-gnueabihf or clang++ --target=arm-linux-gnueabihf to use the libraries from the pi. I asked about this on Stack Overflow and got [this response](https://stackoverflow.com/a/64639629/4262405) It's worth investigating but I need to focus on code.

## Configuration

### YAML

I am using yaml-cpp, you need to do the cmake command with '-DCMAKE_TOOLCHAIN_FILE=../arm.cmake' (or aarch64-clang-toolchain.cmake)

### Boost

I am including boost/units so I am not dependant on the OS for it. It looks like this dosn't include all the stuff needed, I might have to go back to the below.

Formerly I was including all of it and want to leave my notest for instalation here:

#### Installation (if I were using all of boost)

    git submodule init
    git submodule update
    ./bootstrap.sh
[possibly](https://stackoverflow.com/questions/38798671/using-modular-boost-from-github)

### Clang/LLVM

I'm adding a toolchain file aarch64-clang to start using clang, note you still need gcclibs and the debian package `crossbuild-essential-arm64`, `gcc-aarch64-linux-gnu`.

#### Issues

1. Can't use [-mfloat-abi=hard](https://www.keil.com/support/man/docs/armclang_ref/armclang_ref_chr1417451577871.htm) because
1. `gcc-multilib`, and `g++-multilib` seems to get the libs needed. Argh, seems like this dosn't work, gcc-multilib conflicts with clang somehow.

### Things to try

1. [Install gcc 10 on pi](https://www.raspberrypi.org/forums/viewtopic.php?t=273441)
1. [Install pi tools and reference them](https://stackoverflow.com/a/19269715/4262405)
1. [Use clang with a different glibc](https://libcxx.llvm.org/docs/UsingLibcxx.html)

# TODO

## Motor trigger rate

I'm seeing some wierd stuff with the scheduler. I was assuming the usleep(1) in gpio_stepper_control_BCM.hpp:step() would only sleep for 1 microsecond, but it appears it's giving up the scheduler for the whole cycle, so we're getting perfect 800us square waves. (PULSE_RATE is set to 800'000 in nanoseconds). It's probably not a big deal as it just means the schedule PULSE_RATE will have to be half the minimum pulse rate, and that the usleep is acting like a sched_yield() and should probably be used as such. ![scope capture](800'000ns_scheduling_wierdness.png)

## Thread exception catching

It looks like [threads cannot catch eachothers exceptions](https://stackoverflow.com/questions/23835444/unable-to-catch-exception-thrown-by-stdthread-constructor) so I will probably need to not throw the excetion if running without root
