set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(triple arm-linux-gnueabihf)
set(clang-version "-10")
set(ARM_INCLUDE_TRIPPLE "arm-linux-gnueabihf")
set(ARM_INCLUDE_DIRS "-I/usr/${ARM_INCLUDE_TRIPPLE}/include/c++/10/${ARM_INCLUDE_TRIPPLE}/ -I/usr/${ARM_INCLUDE_TRIPPLE}/include/")
message(" trying to include ${CMAKE_CURRENT_LIST_DIR}/arm-clang-tooldefines.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/arm-clang-tooldefines.cmake")