/**
 * @brief Run toe robot app
 *
 * @param argc number of arguments
 * @param argv pointer to arguments
 * @return int
 */

/**
 * I'm getting a defenition of struct sched_param from both
 * 		/usr/arm-linux-gnueabihf/include/bits/sched.h:74 (included by
 * C++ headers string and memory)
 * 		/usr/arm-linux-gnueabihf/include/linux/sched/types.h:7 (included
 * by me to get struct sched_attr) Defining _BITS_TYPES_STRUCT_SCHED_PARAM will
 * allow me to elemenate one
 */
#define _BITS_TYPES_STRUCT_SCHED_PARAM 1

#include <getopt.h>

#include <iostream>

#include "motion_state.hpp"
#include "robot_configuration.hpp"
#include "sensor_state.hpp"

/**
 * @brief a struct of any paramaters to the program that have been parsed by
 * getopt to return to main
 *
 * @param config_file the path to the config file to load
 *
 */
typedef struct config_opts {
  std::string config_file;
} config_opts_t;

static void print_usage(const char* prog) {
  printf("Usage: %s [-c]\n", prog);
  puts(
      "  -c --config_file path to the config file to read from, defaults to "
      "config.yaml\n");
  exit(1);
}

static config_opts_t parse_opts(int argc, char* argv[]) {
  config_opts_t ret_opts;
  ret_opts.config_file.assign("config.yaml");
  while (1) {
    static const struct option lopts[] = {
        {"config_file", required_argument, nullptr, 'c'},
        {NULL, 0, 0, 0},
    };
    int c;

    c = getopt_long(argc, argv, "c:", lopts, NULL);

    if (c == -1) { return ret_opts; }

    switch (c) {
      case 'c':
        ret_opts.config_file.assign(optarg);
        break;
      default:
        print_usage(argv[0]);
        break;
    }
  }
}

int main(int argc, char* argv[]) {
  printf("# %s compilied on %s:%s\n", __FILE__, __DATE__, __TIME__);
  auto options = parse_opts(argc, argv);

  printf("Starting robot\n");

  int moveAmount = 1000;
  char input;
  auto robotConfiguraiton = robotConfiguration(options.config_file);

  /*
   * Note that we call with ! stub.motor.gpio, as the motion state is expecting
   * "true" to mean "use the BCM, not the stub"
   */
  auto motionState = std::make_unique<MotionState>(
      robotConfiguraiton.config["motor"],
      !robotConfiguraiton.config["stub"]["motor"]["gpio"].as<bool>());

  auto sensorState = std::make_unique<SensorState>(
      robotConfiguraiton.config["sensor"]);

  while (1) {
    std::cin >> input;
    switch (input) {
      case 'w':
        printf("moving %d in the +x dir\n", moveAmount);
        motionState->move_to(moveAmount, 0, 0);
        break;
      case 's':
        printf("moving %d in the -x dir\n", moveAmount);
        motionState->move_to(-moveAmount, 0, 0);
        break;
      case 'a':
        printf("moving %d in the +y dir\n", moveAmount);
        motionState->move_to(0, moveAmount, 0);
        break;
      case 'd':
        printf("moving %d in the -y dir\n", moveAmount);
        motionState->move_to(0, -moveAmount, 0);
        break;
      case 'q':
        printf("moving %d about +z\n", moveAmount);
        motionState->move_to(0, 0, moveAmount);
        break;
      case 'e':
        printf("moving %d about -z\n", moveAmount);
        motionState->move_to(0, 0, -moveAmount);
        break;
      case 'r':
        moveAmount += 100;
        printf("increassing move amount to %d\n", moveAmount);
        break;
      case 'f':
        if (moveAmount >= 100) {
          moveAmount -= 100;
          printf("decreasing move amount to %d\n", moveAmount);
        } else {
          printf("won't go negative on motion amount\n");
        }

        break;
      case 'z':
        exit(0);
    }
    for( const SensorState::sensor_values measurment : *sensorState->sensorBuffer){
      std::cout << "gyro x " << measurment.gyro_value[0];
    }
     
  }
}