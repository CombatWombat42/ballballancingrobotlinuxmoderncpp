/**
 * @file motor.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief An interface class spawning a thread that controlls a single motor
 * @version 0.1
 * @date 2020-07-11
 *
 * @copyright Copyright (c) 2020
 *
 * This class will provide a thread that controlls a motor the inital version is
 * targetd at a stepper
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include <linux/sched/types.h>

#include <chrono>
#include <ctime>
#include <exception>
#include <iostream>
#include <thread>

#include "deadline_thread.hpp"

class Motor : DeadlineThread {
 private:
  virtual void run(void) = 0;

 protected:
  /**
   * @brief Construct a new Motor object
   *
   * Anything the run method needs to run will have to be passed in as part of
   * the derived class
   *
   * @param control_rate a timespec defining how often the controller needs to
   * run
   * @param runtime a timespec defining worst case runtime of run method
   */
  Motor(YAML::Node threadNode) : DeadlineThread(threadNode) {}

  /**
   * @brief Set the desired relative position of the motor
   *
   * The motor will move to this relative position as best it can and hold there
   * unless this position is updated. This can be update mid move if a new
   * position is desired.
   *
   * @param desired_relative_position
   */
  virtual void set_desired_relative_position(
      boost::units::quantity<boost::units::si::plane_angle, double>) = 0;
};
#endif  // MOTOR_H_