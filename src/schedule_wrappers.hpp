/**
 * @file schedule_wrappers.hpp
 * @author Sam Povilus (povilus@povil.us)
 * @brief A class to wrap Linux Scheduler calls
 * @version 0.1
 * @date 2020-07-11
 *
 * @copyright Copyright (c) 2020
 *
 */

#include <linux/ioctl.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <sys/syscall.h>
#include <unistd.h>

#ifndef SCHEDULE_WRAPPERS_H_
#define SCHEDULE_WRAPPERS_H_
class ScheduleWrapper {
 public:
  static int sched_setattr(pid_t pid, const struct sched_attr *attr,
                           unsigned int flags) {
    return syscall(__NR_sched_setattr, pid, attr, flags);
  }

  static int sched_getattr(pid_t pid, struct sched_attr *attr,
                           unsigned int size, unsigned int flags) {
    return syscall(__NR_sched_getattr, pid, attr, size, flags);
  }

  static int sched_yield() {
    return syscall(__NR_sched_yield);
  }
};
#endif  // SCHEDULE_WRAPPERS_H_