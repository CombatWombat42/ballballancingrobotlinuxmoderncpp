/**
 * @file motion_state.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief take in steering commands and output motor position commands
 * @version 0.1
 * @date 2020-07-18
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef MOTION_STATE_H_
#define MOTION_STATE_H_
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/angle/revolutions.hpp>
#include <boost/units/systems/si.hpp>
#include <vector>

#include "stepper_motor_TB6600.hpp"
#include "yaml-cpp/yaml.h"

class MotionState {
 private:
  boost::units::quantity<boost::units::si::plane_angle, double>
      degrees_per_step = 1.8 * boost::units::si::radians;

  std::chrono::nanoseconds stepper_pulse_rate =
      std::chrono::nanoseconds(1'000'000);
  std::chrono::nanoseconds stepper_computation_time =
      std::chrono::nanoseconds(10'000);

  StepperMotorTB6600 motor0;
  StepperMotorTB6600 motor1;
  StepperMotorTB6600 motor2;

 public:
  /**
   * @brief Construct a new Motion State object
   *
   * Initalize motor
   *
   * @param motion_control_node
   */
  MotionState(YAML::Node motion_control_node, bool bcm)
      : motor0(motion_control_node["0"]["dir_pin"].as<int>(),
               motion_control_node["0"]["pulse_pin"].as<int>(),
               degrees_per_step,
               0,
               motion_control_node["thread"],
               bcm),
        motor1(motion_control_node["1"]["dir_pin"].as<int>(),
               motion_control_node["1"]["pulse_pin"].as<int>(),
               degrees_per_step,
               0,
               motion_control_node["thread"],
               bcm),
        motor2(motion_control_node["2"]["dir_pin"].as<int>(),
               motion_control_node["2"]["pulse_pin"].as<int>(),
               degrees_per_step,
               0,
               motion_control_node["thread"],
               bcm) {}
  void move_to(int X, int Y, int spin) {
    std::vector<float> motorValues;
    motorValues.push_back((
        ((-1.0 * (sin(M_PI / 3.0) * X)) + (cos(M_PI / 3.0) * Y) + spin) / 3.0));
    motorValues.push_back(((-Y + spin) / 3.0));
    motorValues.push_back(
        (((sin(M_PI / 3.0) * X) + (cos(M_PI / 3.0) * Y) + spin) / 3.0));
    printf(
        "transformed X: %d Y: %d rot: %d into m0: %05.05f m1: "
        "%05.05f m2: %05.05f\n",
        X,
        Y,
        spin,
        motorValues.at(0),
        motorValues.at(1),
        motorValues.at(2));
    motor0.add_relative_position(motorValues.at(0) * boost::units::si::radians);
    motor1.add_relative_position(motorValues.at(1) * boost::units::si::radians);
    motor2.add_relative_position(motorValues.at(2) * boost::units::si::radians);
  }
};
#endif  // MOTION_STATE_H