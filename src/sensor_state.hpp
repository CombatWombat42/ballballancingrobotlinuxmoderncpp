/**
 * @file sensor_state.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief manage thread(s) for an accelerometer and gyro
 * @version 0.1
 * @date 2020-11-03
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef SENSOR_STATE_H_
#define SENSOR_STATE_H_

#include <boost/circular_buffer.hpp>
#include <chrono>

#include "accelerometer.hpp"
#include "deadline_thread.hpp"
#include "gyro.hpp"

/**
 * @brief The Sensor State class will hold the accelerometer and gyro thread(s)
 * and the data from the sensors
 *
 */
class SensorState : DeadlineThread {
 public:
  struct sensor_values {
    int16_t gyro_value[3];
    int16_t accelerometer_value[3];
    std::chrono::high_resolution_clock::time_point time_taken;
  };

 private:
  /**
   * @brief the accelerometer to collect data from
   *
   */
  std::unique_ptr<Accelerometer> accelerometer;
  /**
   * @brief the gyro to collect data from
   *
   */
  std::unique_ptr<Gyro> gyro;

  /**
   * @brief collect data from the sensors and store it
   *
   */
  void run() {
    while (1) {
      if (gyro) { gyro->readGyro(); }
      if (accelerometer) { accelerometer->readAccelerometer(); }
      if (sensorBuffer) {
        sensorBuffer->push_back({gyro->get_latest_X(),
                                 gyro->get_latest_Y(),
                                 gyro->get_latest_Z(),
                                 accelerometer->get_latest_X(),
                                 accelerometer->get_latest_Y(),
                                 accelerometer->get_latest_Z(),
                                 std::chrono::high_resolution_clock::now()});
      }
    }
  }

 public:
 /**
  * @brief Construct a new Sensor State object this creates a thread that reads the spi accelerometer and gyro
  * 
  * @param sensorNode a YAML node descriping the paths to use for the sensors, the scailing factor, the debugging ring buffer depth and the thread timing 
  */
  SensorState(YAML::Node sensorNode) : DeadlineThread(sensorNode["thread"]) {
    gyro = std::make_unique<Gyro>(
        sensorNode["gyro"]["spi_file"].as<std::string>());
    accelerometer = std::make_unique<Accelerometer>(
        sensorNode["acceleromoeter"]["spi_file"].as<std::string>());
    sensorBuffer = std::make_shared<boost::circular_buffer<sensor_values>>(
        sensorNode["debug"]["ring_buffer_depth"].as<int>());
    gyro->initalizeGyro();
    accelerometer->initalizeAccelerometer();
  }

    /**
   * @brief a ring buffer that stores sensor values for debugging
   *
   */
  std::shared_ptr<boost::circular_buffer<sensor_values>> sensorBuffer;


};
#endif  // SENSOR_STATE_H_