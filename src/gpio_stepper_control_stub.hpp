/**
 * @file gpio_stepper_control_stub.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief
 * @version 0.1
 * @date 2020-07-21
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef GPIO_STEPPER_CONTROL_BCM_H_STUB
#define GPIO_STEPPER_CONTROL_BCM_H_STUB

#include <stdint.h>
#include <sys/mman.h>

#include "gpio_stepper_control_interface.hpp"

class GPIOStepperControlStub : public GPIOStepperControlInterface {
 public:
  GPIOStepperControlStub() {}
  void step(void) {}

  void set_dir(dir dir) {}
};
#endif  // GPIO_STEPPER_CONTROL_BCM_H_STUB