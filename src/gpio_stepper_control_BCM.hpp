/**
 * @file gpio_stepper_control_BCM.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief
 * @version 0.1
 * @date 2020-07-13
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef GPIO_STEPPER_CONTORL_BCM_H_
#define GPIO_STEPPER_CONTORL_BCM_H_

#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "gpio_stepper_control_interface.hpp"

class GPIOStepperControlBCM : public GPIOStepperControlInterface {
 private:
  static const int PAGE_SIZE = (4 * 1024);
  static const int BLOCK_SIZE = (4 * 1024);
  static const uint32_t BCM2711_PERIPHERAL_BASE = 0xFE00'0000;
  static const uint32_t BCM2708_PERIPHERAL_BASE = 0x2000'0000;
  static const uint32_t BCM2711_GPIO_BASE = BCM2711_PERIPHERAL_BASE + 0x20'0000;
  static const uint32_t BCM2708_GPIO_BASE = BCM2708_PERIPHERAL_BASE + 0x20'0000;
  static const uint8_t BCM_PINS_PER_FUNCTION_SEL_REG = 10;
  static const uint8_t BCM_PIN_FUNCITON_MASK = 0x7;
  static const uint8_t BCM_FUNCITON_BITS_PER_PIN = 3;
  static const uint8_t BCM_FUNCTION_AS_INPUT_VAL = 0;
  static const uint8_t BCM_FUNCTION_AS_OUTPUT_VAL = 1;
  static const uint8_t BCM_GPIO_SET_REG_OFFSET = 7;
  static const uint8_t BCM_GPIO_CLEAR_REG_OFFSET = 10;
  static const uint8_t BCM_SET_CLR_BANK_SIZE = 32;
  // I/O access
  volatile unsigned *gpio;

  const uint8_t stepPin;
  const uint8_t dirPin;
  const bool clockwiseHigh;

  // GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or
  // SET_GPIO_ALT(x,y) (this is because the inp clears it, so you can use a bare
  // or) http://www.pieter-jan.com/node/15

  void set_pin_as_input(uint8_t pinNum) {
    *(gpio + ((pinNum) / BCM_PINS_PER_FUNCTION_SEL_REG)) &=
        ~(BCM_PIN_FUNCITON_MASK << (((pinNum) % BCM_PINS_PER_FUNCTION_SEL_REG) *
                                    BCM_FUNCITON_BITS_PER_PIN));
  }

  void set_pin_as_output(uint8_t pinNum) {
    /*
     * Set as input to clear bits
     */
    set_pin_as_input(pinNum);
    *(gpio + ((pinNum) / BCM_PINS_PER_FUNCTION_SEL_REG)) |=
        (BCM_FUNCTION_AS_OUTPUT_VAL
         << (((pinNum) % BCM_PINS_PER_FUNCTION_SEL_REG) *
             BCM_FUNCITON_BITS_PER_PIN));
  }

  /*
#define SET_GPIO_ALT(g, a)  \
  *(gpio + (((g) / 10))) |= \
      (((a) <= 3 ? (a) + 4 : (a) == 4 ? 3 : 2) << (((g) % 10) * 3))
*/
  void output_high(uint8_t pinNum) {
    int bank = 0;
    if (pinNum >= BCM_SET_CLR_BANK_SIZE) { bank = 1; }
    *(gpio + BCM_GPIO_SET_REG_OFFSET + bank) =
        (1 << (pinNum % BCM_SET_CLR_BANK_SIZE));
  }

  void output_low(uint8_t pinNum) {
    int bank = 0;
    if (pinNum >= BCM_SET_CLR_BANK_SIZE) { bank = 1; }
    *(gpio + BCM_GPIO_CLEAR_REG_OFFSET + bank) =
        1 << (pinNum % BCM_SET_CLR_BANK_SIZE);
  }

  /*
  #define GET_GPIO(g) (*(gpio + 13) & (1 << g))  // 0 if LOW, (1<<g) if HIGH
  #define GPIO_PULL *(gpio + 37)      // Pull up/pull down
  #define GPIO_PULLCLK0 *(gpio + 38)  // Pull up/pull down clock
  */
 public:
  GPIOStepperControlBCM(int stepPin, int dirPin, bool invertDir = false)
      : stepPin(stepPin), dirPin(dirPin), clockwiseHigh(invertDir) {
    int mem_fd;

    void *gpio_map;
    /* open /dev/mem */
    if ((mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
    }

    /* mmap GPIO */
    gpio_map = mmap(
        NULL,                    // Any adddress in our space will do
        BLOCK_SIZE,              // Map length
        PROT_READ | PROT_WRITE,  // Enable reading & writting to mapped memory
        MAP_SHARED,              // Shared with other processes
        mem_fd,                  // File to map
        BCM2711_GPIO_BASE        // Offset to GPIO peripheral
    );

    close(mem_fd);  // No need to keep mem_fd open after mmap

    if (gpio_map == MAP_FAILED) {
      printf("mmap error %lld\n", (long long int)gpio_map);  // errno also set!
      throw memory_mapping_exception;
    }

    // Always use volatile pointer!
    gpio = (volatile unsigned *)gpio_map;

    set_pin_as_output(stepPin);
    set_pin_as_output(dirPin);
  }

  void step(void) {
    output_low(stepPin);
    /*
     *  It seems like as long as our step rate dosn't get into the high KHz
     * range this works pretty well. This should proablly not be offered as a
     * function at this level because of the need for this usleep, but I am
     * going to live with it for now.
     */
    usleep(100);
    output_high(stepPin);
  }

  void set_dir(dir dir) {
    if (clockwiseHigh) {
      if (dir == CCW) {
        output_low(dirPin);
      } else {
        output_high(dirPin);
      };
    } else {
      if (dir == CW) {
        output_low(dirPin);
      } else {
        output_high(dirPin);
      };
    }
  }

  class MemoryMappingException : public std::exception {
    /**
     * @brief Something is happening with mapping /dev/mem
     *
     * @return const char* the string telling the user what happened
     */
    virtual const char *what() const throw() {
      return "Unable to access /dev/mem, possibly need to be root?";
    }
  } memory_mapping_exception;
};
#endif  // GPIO_STEPPER_CONTORL_BCM_H_