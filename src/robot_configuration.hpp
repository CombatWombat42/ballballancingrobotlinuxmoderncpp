/**
 * @file robot_configuration.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief Holds configuration  values for the robot
 * @version 0.1
 * @date 2020-07-19
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef ROBOT_CONFIGURATION_H_
#define ROBOT_CONFIGURATION_H_

#include <iostream>
#include <map>

#include "yaml-cpp/yaml.h"

class robotConfiguration {
 private:
  /* data */
 public:
  YAML::Node config;
  robotConfiguration(std::string);
  ~robotConfiguration();
};

robotConfiguration::robotConfiguration(std::string filename) {
  config = YAML::LoadFile(filename);
}

robotConfiguration::~robotConfiguration() {}

#endif  // ROBOT_CONFIGURATON_H_