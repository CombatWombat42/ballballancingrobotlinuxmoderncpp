/**
 * @file brushed_motors.hpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-07-03
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include <map>
#include <iostream>
#include <math.h>

/**
 * @brief class to setup and control motors via pwm using sysfs interface
 * 
 * Asumptions:
 * 
 * 1. All pwm devices have been put into a single directory
 * 
 */
class Motors
{
public:
    typedef enum MOTORS
    {
        MOTOR0,
        MOTOR1,
        MOTOR2
    };

    typedef enum DIRECTION
    {
        CCW = 0,
        CW = 1
    };

private:
    inline static const std::string PERIOD_FILE = "/period";
    inline static const std::string DUTY_CYCLE_FILE = "/duty_cycle";
    inline static const std::string ENABLE_FILE = "/enable";
    inline static const std::string DIR_FILE = "/value";
    static const int SEC_TO_NSEC = 1000000000;
    uint64_t mPeriodNs;
    std::vector<std::string> mDirs;

    std::map<MOTORS, std::string> MOTOR_FILE_MAP;
    std::map<MOTORS, std::string> DIR_FILE_MAP;

    /**
 * @brief the max power that give a change in speed (we aren't using nearly all of the torque of these motors)
 * 
 */
    static constexpr float MAX_DUTY_CYCLE = 1.0;
    /**
     * @brief the min power that give any movement
     * 
     */
    static constexpr float MIN_DUTY_CYCLE = 0.5;

    /**
 * @brief Get the directories 
 * 
 * based on https://stackoverflow.com/a/46589798/4262405
 * 
 * @param baseDir
 * @return std::vector<std::string> 
 */
    std::vector<std::string> get_directories(const std::string &baseDir)
    {
        std::vector<std::string> r;
        for (auto &p : std::filesystem::directory_iterator(baseDir))
            if (p.is_directory())
                r.push_back(p.path().string());
        return r;
    }

    /**
 * @brief update the drive frequency of the motors using a period
 * 
 * @param motorPath the path to the file to update
 * @param periodNs the period in nanoseconds
 */
    void update_period(const std::string motorPath, uint64_t periodNs)
    {
        std::ofstream file;
        file.open(motorPath);
        file << periodNs;
        file.close();
    }

    void set_duty_cycle(float duty_cycle_percent, MOTORS motorNum)
    {
        printf("setting motor %d to %f\n", motorNum, duty_cycle_percent );
        auto dutyCycleNs = static_cast<uint64_t>(mPeriodNs * (duty_cycle_percent / 100.0));
        update_period(MOTOR_FILE_MAP[motorNum] + DUTY_CYCLE_FILE, dutyCycleNs);
    }

    void set_direction(DIRECTION dir, MOTORS motorNum)
    {
        printf("setting %d to %d\n", motorNum, dir );
        update_period(DIR_FILE_MAP[motorNum] + DIR_FILE, dir);
    }

    void enable(bool enabled)
    {
        int enableDisable;
        if (enabled)
        {
            enableDisable = 1;
        }
        else
        {
            enableDisable = 0;
        }

        for (auto periodDir : mDirs)
        {
            update_period(periodDir + ENABLE_FILE, enableDisable);
        }
    }

public:
    Motors(float frequency, std::string basePath = "/opt/robot/motors/")
    {
        MOTOR_FILE_MAP[MOTOR0] = (basePath + "/motor0/");
        MOTOR_FILE_MAP[MOTOR1] = (basePath + "/motor1/");
        MOTOR_FILE_MAP[MOTOR2] = (basePath + "/motor2/");

        DIR_FILE_MAP[MOTOR0] = (basePath + "/dir0/");
        DIR_FILE_MAP[MOTOR1] = (basePath + "/dir1/");
        DIR_FILE_MAP[MOTOR2] = (basePath + "/dir2/");

        mPeriodNs = static_cast<uint64_t>((1.0 / frequency) * SEC_TO_NSEC);
        mDirs = get_directories(basePath);
        for (auto periodDir : mDirs)
        {
            // Update duty cycle to be 0 before trying to set period as the driver will balk if you try to set a period lower than the duty cycle
            update_period(periodDir + DUTY_CYCLE_FILE, 0);
            update_period(periodDir + PERIOD_FILE, mPeriodNs);
            update_period(periodDir + ENABLE_FILE, 1);
        }
    }

    /**
 * @brief Set the motor speeds
 * 
 *  Anyone looking at this code, i know this method is incredibly naive i need to get code working to test hardware
 *  TODO: make this much much cleaner
 * 
 * @param m0 speed desired for motor 0, in the range of 1.0 - -1.0
 * @param m1 speed desired for motor 1, in the range of 1.0 - -1.0
 * @param m2 speed desired for motor 2, in the range of 1.0 - -1.0
 */
    void set_m0m1m2(float m0, float m1, float m2)
    {
        if ((m0 > 1.0) || (m1 > 1.0) || (m2 > 1.0) || (m0 < -1.0) || (m1 < -1.0) || (m2 < -1.0))
        {
            printf("Motor set out of range m0: %03.03f m1: %03.03f m2: %03.03f\n", m0, m1, m2);
        }
        if (m0 < 0.0)
        {
            set_direction(CW, MOTOR0);
        }
        else
        {
            set_direction(CCW, MOTOR0);
        }
        if (m1 < 0.0)
        {
            set_direction(CW, MOTOR1);
        }
        else
        {
            set_direction(CCW, MOTOR1);
        }
        if (m2 < 0.0)
        {
            set_direction(CW, MOTOR2);
        }
        else
        {
            set_direction(CCW, MOTOR2);
        }
        auto m0value = (((MIN_DUTY_CYCLE) + ((MAX_DUTY_CYCLE - MIN_DUTY_CYCLE) * abs(m0)))*100.0);
        auto m1value = (((MIN_DUTY_CYCLE) + ((MAX_DUTY_CYCLE - MIN_DUTY_CYCLE) * abs(m1)))*100.0);
        auto m2value = (((MIN_DUTY_CYCLE) + ((MAX_DUTY_CYCLE - MIN_DUTY_CYCLE) * abs(m2)))*100.0);
        printf("max duty cycle %f min duty cycle %f m0 %f m0val %f\n0",MAX_DUTY_CYCLE,MIN_DUTY_CYCLE,m0,m0value);
        printf("max duty cycle %f min duty cycle %f m1 %f m0val %f\n0",MAX_DUTY_CYCLE,MIN_DUTY_CYCLE,m1,m1value);
        printf("max duty cycle %f min duty cycle %f m2 %f m0val %f\n0",MAX_DUTY_CYCLE,MIN_DUTY_CYCLE,m2,m2value);
        
        set_duty_cycle((m0value),MOTOR0);
        set_duty_cycle((m1value),MOTOR1);
        set_duty_cycle((m2value),MOTOR2);
    }
};