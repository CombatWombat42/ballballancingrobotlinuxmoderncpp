#include "sensor.hpp"
#include "LSM9DS0.hpp"
class Accelerometer : public Sensor
{
public:
    Accelerometer(std::string filename) : Sensor(filename)
    {
        
    }
    void initalizeAccelerometer()
    {
        if (mDebug)
        {
            printf("# initalizing gyro\n");
        }
        mStartAddr = LSM9DS0::ACC_DATA_START_REG_ADDR;
        tx[0] = (LSM9DS0::ACC_RATE_ENABLE_REG_ADDR);
        tx[1] = ((LSM9DS0::ACC_50_HZ) | ((LSM9DS0::ACC_X_EN | LSM9DS0::ACC_Y_EN | LSM9DS0::ACC_Z_EN)));
        transfer(2);
    }
    void readAccelerometer()
    {
        read_sensor_data();
    }
};