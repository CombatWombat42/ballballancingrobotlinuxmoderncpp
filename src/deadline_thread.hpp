/**
 * @file deadline_thread.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief Manage a SCHED_DEADLINE thread
 * @version 0.1
 * @date 2020-11-03
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef DEADLINE_THREAD_H_
#define DEADLINE_THREAD_H_
#include <ctime>

#include "schedule_wrappers.hpp"
#include "yaml-cpp/yaml.h"

class DeadlineThread {
  /**
   * @brief This is the main loop the motor performs
   *
   * This method  should not return, but it shold call sched_yield when it is
   * done with this loops tasks. See presentations on SCHED_DEADLINE for
   * details. https://youtu.be/TDR-rgWopgM?t=1124
   * https://elinux.org/images/f/fe/Using_SCHED_DEADLINE.pdf page 34
   *
   */
  virtual void run() = 0;

  /**
   * @brief Setup the thread to be scheduled by the deadline scheduler and run
   * the run method
   *
   * @param control_rate a timespec defining how often the controller needs to
   * run
   * @param runtime a timespec defining worst case runtime of run method
   */
  void start_thread(timespec control_rate, timespec runtime) {
    int ret;
    struct sched_attr attr;
    ret = ScheduleWrapper::sched_getattr(0, &attr, sizeof(attr), 0);
    if (ret < 0) {
      perror("Unable to get scheduler attributes");
      throw scheduler_exception;
    }
    attr.sched_policy = SCHED_DEADLINE;
    attr.sched_runtime = (runtime.tv_nsec +
                          (std::chrono::duration_cast<std::chrono::nanoseconds>(
                               std::chrono::seconds(runtime.tv_sec))
                               .count()));
    attr.sched_deadline = control_rate.tv_nsec +
                          (std::chrono::duration_cast<std::chrono::nanoseconds>(
                               std::chrono::seconds(control_rate.tv_sec)))
                              .count();
    ret = ScheduleWrapper::sched_setattr(0, &attr, 0);
    if (ret < 0) {
      perror(
          "Motor thread initilization failed, unable to set schedule"
          "attributes");
      throw scheduler_exception;
    }
    run();
    throw control_returned_exception;
  }

 protected:
  /**
   * @brief Construct a new Deadline Thread object
   *
   * @param control_rate how often this thread should run in nano seconds
   * @param runtime how long this thread takes to run in the worst case in nano
   * seconds
   */
  DeadlineThread(YAML::Node threadNode) {
    timespec control_rate_ts = {.tv_sec = 0,
                                .tv_nsec = static_cast<__syscall_slong_t>(
                                    threadNode["rate_ns"].as<int>())};

    timespec computation_time_ts = {
        .tv_sec = 0,
        .tv_nsec = static_cast<__syscall_slong_t>(
            threadNode["computation_time_ns"].as<int>())};
    auto deadlineThread = std::thread(&DeadlineThread::start_thread,
                                      this,
                                      control_rate_ts,
                                      computation_time_ts);
    deadlineThread.detach();
  }

 public:
  /**
   * @brief An exception to throw when the run method returns unexpectedly
   *
   */
  class ControlReturnedException : public std::exception {
    /**
     * @brief Let the user know they should not have returned from "run"
     *
     * @return const char* the string telling the user they are dumb
     */
    virtual const char* what() const throw() {
      return "Returned from a function that was expected to run forever";
    }
  } control_returned_exception;

  class SchedulerException : public std::exception {
   public:
    /**
     * @brief Something is happening with the sceduler
     *
     * @return const char* the string telling the user what happened
     */
    virtual const char* what() const throw() {
      return "Scheduler failed";
    }
  } scheduler_exception;
};

#endif  // DEADLINE_THREAD_H_