#include <stdint.h>

#ifndef LSM9DS0_H
#define LSM9DS0_H
class LSM9DS0
{
public:
    static const uint8_t SPI_READ = (0x1 << 7);
    static const uint8_t SPI_AUTO_ADDR_INC = (0x1 << 6);

    static const uint8_t ACC_RATE_ENABLE_REG_ADDR = (0x20);
    static const uint8_t GYRO_ENABLE_REG = (0x20);
    static const uint8_t ACC_DATA_START_REG_ADDR = (0x28);
    static const uint8_t GYRO_DATA_START_REG_ADDR = (0x28);

    static const uint8_t ACC_POWER_DOWN = (0x0 << 4);
    static const uint8_t ACC_3P125_HZ = (0x1 << 4);
    static const uint8_t ACC_6p25_HZ = (0x2 << 4);
    static const uint8_t ACC_12p5_HZ = (0x3 << 4);
    static const uint8_t ACC_25_HZ = (0x4 << 4);
    static const uint8_t ACC_50_HZ = (0x5 << 4);
    static const uint8_t ACC_100_HZ = (0x6 << 4);
    static const uint8_t ACC_200_HZ = (0x7 << 4);
    static const uint8_t ACC_400_HZ = (0x8 << 4);
    static const uint8_t ACC_800_HZ = (0x9 << 4);
    static const uint8_t ACC_1600_HZ = (0xa << 4);

    static const uint8_t ACC_X_EN = (0x1 << 0);
    static const uint8_t ACC_Y_EN = (0x1 << 1);
    static const uint8_t ACC_Z_EN = (0x1 << 2);

    static const uint8_t GYRO_ENABLE = (0x1 << 3);

    static const uint8_t GYRO_X_EN = (0x1 << 0);
    static const uint8_t GYRO_Y_EN = (0x1 << 1);
    static const uint8_t GYRO_Z_EN = (0x1 << 2);
};
#endif //LSM9DS0_H