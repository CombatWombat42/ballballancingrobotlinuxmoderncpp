/**
 * @file gpio_stepper_control_interface.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief
 * @version 0.1
 * @date 2020-07-21
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef GPIO_STEPPER_CONTROL_INTERFACE_H_
#define GPIO_STEPPER_CONTROL_INTERFACE_H_

#include <stdint.h>
#include <sys/mman.h>

class GPIOStepperControlInterface {
 public:
  enum dir { CCW = 0, CW = 1 };
  GPIOStepperControlInterface() {}
  virtual ~GPIOStepperControlInterface() {}
  virtual void step(void) = 0;

  virtual void set_dir(dir dir) = 0;
};
#endif  // GPIO_STEPPER_CONTROL_INTERFACE_H_