/**
 * @file stepper_motor_TB6600.hpp
 * @author Sam Povilus (robot@povil.us
 * @brief Controll a motor using a TB6600 connected to GPIO
 * @version 0.1
 * @date 2020-07-11
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef STEPPER_MOTOR_TB6600_H_
#define STEPPER_MOTOR_TB6600_H_
#include "gpio_stepper_control_BCM.hpp"
#include "gpio_stepper_control_interface.hpp"
#include "gpio_stepper_control_stub.hpp"
#include "motor.hpp"

class StepperMotorTB6600 : public Motor {
 private:
  /**
   * @brief the stepper motor interface for this motor
   *
   */
  std::unique_ptr<GPIOStepperControlInterface> stepperController;

  /**
   * @brief the number of degrees the motor will move per pulse of the pulse pin
   *
   */
  boost::units::quantity<boost::units::si::plane_angle, double>
      degrees_per_step;

  /**
   * @brief identifier of what motor i am controlling, used for debgging only
   *
   */
  int MotorNum;

  /**
   * @brief the current relative postion
   *
   */
  boost::units::quantity<boost::units::si::plane_angle, double>
      desired_relative_position;

  /**
   * @brief see if we need to step the motor, and what direction
   *
   * Set the dir pin to high or low depending on the desired direction, and make
   * a step if nessicary
   *
   */
  void run(void) {
    while (1) {
      if (std::abs(desired_relative_position.value()) <
          degrees_per_step.value()) {
        // printf("Cant move less than %f degrees\n", degrees_per_step.value());
      } else {
        // printf("Motor %d need to move %f \n",
        //        MotorNum,
        //        desired_relative_position.value());
        if (desired_relative_position.value() > 0.0) {
          desired_relative_position -= degrees_per_step;
          stepperController->set_dir(GPIOStepperControlInterface::CCW);
          stepperController->step();
        } else {
          desired_relative_position += degrees_per_step;
          stepperController->set_dir(GPIOStepperControlInterface::CW);
          stepperController->step();
        }
      }
      sched_yield();
    }
  }

 public:
  /**
   * @brief Construct a new Stepper Motor TB6600 object
   *
   * Sets itself up to be run every CYCLE_TIME_NSEC and takes
   * CYCLE_RUNTIME_NSEC to run in the worst case
   *
   * @param dir_pin the gpio pin to which the direction signal is connected
   * @param pulse_pin the gpio pin to which the pulse signal is connected
   * @param degrees_per_step the number of degrees the motor will move per
   * pulse of the pulse pin defaults to 1.8 degrees as most steppers are 200
   * pulse/revolution
   */
  StepperMotorTB6600(int dir_pin,
                     int pulse_pin,
                     boost::units::quantity<boost::units::si::plane_angle,
                                            double> degrees_per_step2,
                     int motor_num,
                     YAML::Node threadNode,
                     bool bcm = true)
      : Motor(threadNode), MotorNum(motor_num) {
    printf("Initalizing motor with dir pin %d and pulse pin %d\n",
           dir_pin,
           pulse_pin);
    degrees_per_step = degrees_per_step2;
    if (bcm) {
      stepperController =
          std::make_unique<GPIOStepperControlBCM>(pulse_pin, dir_pin);
    } else {
      printf("\x1B[31mWarning: Using stub motor\033[0m\n");
      stepperController = std::make_unique<GPIOStepperControlStub>();
    }
    //'    desired_relative_position.set(0.0);
  }

  void set_desired_relative_position(
      boost::units::quantity<boost::units::si::plane_angle, double>
          desired_relative_position2) {
    desired_relative_position = desired_relative_position2;
  }

  void add_relative_position(
      boost::units::quantity<boost::units::si::plane_angle, double>
          desired_relative_position2) {
    desired_relative_position += desired_relative_position2;
  }
};
#endif  // STEPPER_MOTOR_TB6600_H_