#!/usr/bin/env bash

#apparently we have to set the pins to pwm now?
config-pin P9.14 pwm
config-pin P9.22 pwm
config-pin P8.13 pwm

#apparently you have to enable them before you can set polarity
echo 10000 > /opt/robot/motors/motor0/period
echo 10000 > /opt/robot/motors/motor1/period  
echo 10000 > /opt/robot/motors/motor2/period  


echo "inversed" > /opt/robot/motors/motor0/polarity
echo "inversed" > /opt/robot/motors/motor1/polarity
echo "inversed" > /opt/robot/motors/motor2/polarity
